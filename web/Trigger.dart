import 'GameController.dart';
import 'dart:io';

class Trigger{

  GameController gameController;
  
  String type;
  int seconds;
  bool active;

  Trigger(String type, GameController controller){
    this.type = type;
    resetTimer();
    gameController = controller;
  }
  
  void startTimer(){
      resetTimer();
      active = true;
      while(gameController.gameOver() == false && active == true){
        sleep(const Duration(seconds:1));
        incrementTimer();
      }
    
  }

  void resetTimer(){
    seconds = 0;
    active = false;
  }

  bool handleCoin(){
    return false;
  }

  bool handleObstacle(){
    return false;
  }

  bool handleKeyDown() {
    return false;
  }

  void incrementTimer(){
    seconds++;
  }
}