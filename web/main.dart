import 'dart:html';
import 'GameController.dart';
import 'View.dart';

void main() {
  
  final field = querySelector('#gamefield');
  GameController controller = GameController();
  controller.setMaxScreenX(field.getBoundingClientRect().width.toInt());
  controller.setMaxScreenY(field.getBoundingClientRect().height.toInt());
  
  View view = View();
  controller.startGame();
  
  
  
}
