import 'Object.dart';
import 'Position.dart';

class Obstacle extends Object{

    String type;
    
    Obstacle(String type, Position position, bool canMove) : super(position, canMove){
        this.type = type;
    }
    
    String getType(){
        return type;
    }
    
}