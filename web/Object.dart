import 'Position.dart';

class Object{

  bool canMove;
  bool visible;
  int speed;

  Position position;

  void hide(){
    visible = false;
  }

  void show(){
    if(position.isValid()){
      visible = true;
    }
  }

  Object(Position position, bool canMove){
      this.canMove = canMove;
      visible = false;
      this.position = position;
  }

  Position getPosition(){
    return position;
  }

  void setSpeed(int value){
    speed = value;
  }
  
  bool isVisible(){
    return visible;
  }
}