import 'Position.dart';
import 'Object.dart';

class Coin extends Object{
    
    int value;
    
    Coin(int value, Position position, bool canMove) : super(position,canMove){
        this.value = value;
    }
    
    int getValue(){
        return value;
    }
    

}