import 'Car.dart';
import 'Track.dart';
import 'Trigger.dart';
import 'View.dart';

class GameController{

  View view;
  Track track;
  
  int speed;
  int maxScreenX;
  int maxScreenY;

  Trigger timer;
  Trigger goldcoin;
  Trigger keyDown;
  Trigger obstacle;
  
  GameController(){
    view = View(this);
    timer = Trigger('timer',this);
    goldcoin = Trigger('coin',this);
    keyDown = Trigger('key',this);
    obstacle = Trigger('obstacle',this);
  }

  void startGame(){

  }

  void newGame(Track track){
    this.track = track;
  }

  bool gameOver(){
    return false;
  }

  void moveCar(){

  }

  void increaseSpeed(){
    speed++;
  }

  void resetSpeed(){
    speed = 1;
  }
  
  void nextLevel(){

  }
  
  void setMaxScreenX(int value){
      maxScreenX = value;
  }
  
  void setMaxScreenY(int value){
      maxScreenY = value;
  }

}
