import 'dart:collection';
import 'dart:io';
import 'dart:async';

import 'Object.dart';

class Track{

  var objects = new List<Object>();
  int id;
  int minTime;
  int minCoins;
  int maxCollisions;

  Track(int id){
    // load track values from txt/xml file
    if(File('Track'+id.toString()+'.txt').exists() == null){
      new Exception('No Levelfile found.');
    }
    else{
      String readAsLines = new File('Track'+id.toString()+'.txt').readAsString().toString();
      var buf = readAsLines.split('\n');
      if(buf.length < 3){
        new Exception('Invalid Leveldata.');
      }
      else{
        minTime = int.parse(buf[0]);
        minCoins = int.parse(buf[1]);
        maxCollisions = int.parse(buf[2]);
      }
    }
    this.id = id;
  }

  void addObject(Object o){
    objects.add(o);
  }

  bool deleteObject(Object o){
    if(objects.contains(o)){
      objects.remove(o);
      return true;
    }
    else{
      return false;
    }
  }


}