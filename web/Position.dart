import 'dart:html';

class Position{

   int X;
   int Y;

   Position(int x, int y){
     X = x;
     Y = y;
   }

   void moveRight(int steps){
     if(new Position(X+steps,Y).isValid()){
       X += steps;
     }
   }

   void moveLeft(int steps){
     if(new Position(X-steps,Y).isValid()){
       X -= steps;
     }
   }

   void moveForward(int steps){
     if(new Position(X,Y+steps).isValid()){
       Y += steps;
     }
   }

   bool isValid(){
     return true; // Noch nicht richtig implementiert, checke Screen Bounds (Const in Controller einfügen)
   }
   
   int getX(){
       return X;
   }
   
   int getY(){
       return Y;
   }
}