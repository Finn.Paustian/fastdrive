import 'dart:html';
import 'GameController.dart';

class View{

  GameController gameController;

  HtmlElement startButton;
  String user;
  String password;
  
  View(GameController controller){
      gameController = controller;
  }

  void updateView(){

  }

  void generateTrack(){

  }

  void generateCar(){

  }

  void generateObstacles(){

  }

  void generateCoins(){

  }

  void generateGame(){

  }

  void show(){

  }

  void nextLevel(){

  }

  void mainMenu(){

  }

  void closeGame(){
    
  }



}